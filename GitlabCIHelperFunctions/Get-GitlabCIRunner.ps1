﻿function Get-GitlabCIRunner
{
    [CmdletBinding()]
    param(
        $DestinationPath = $pwd,

        [ValidateSet(32,64)]
        $Version = 64
    )
    
    if (Test-Path $DestinationPath -PathType Container)
    {
        $VersionTable = @{32='386';64='amd64'}
        $url = 'https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-windows-{0}.exe' -f $VersionTable[$Version]      
        $FilePath = Join-Path -Path $DestinationPath -ChildPath ('gitlab-ci-runner-{0}.exe' -f $Version)
        Invoke-WebRequest -Uri $url -Method Get -OutFile $FilePath

        [pscustomobject]@{
            Version = $Version
            FilePath = $FilePath
        }
    }
    else
    {
        Write-Error 'Please provide a valid path to the runner folder'
        return
    }  
}

