﻿
#
# Module Manifest for Module 'GitlabCIHelperFunctions
#
# Generated: 2016-02-07
#

@{

# Module Loader File
ModuleToProcess = 'loader.psm1'

# Version Number
ModuleVersion = '1.0'

# Unique Module ID
GUID = '8f1b487c-28fc-425f-8641-28fd30cc6cc4'

# Module Author
Author = 'Sebastian Neumann'

# Company
CompanyName = ''

# Copyright
Copyright = '(c) 2016 Sebastian Neumann. All rights reserved.'

# Module Description
Description = 'Provides a set of helper functions to help with gitlab ci'

# Minimum PowerShell Version Required
PowerShellVersion = '3.0'

# Name of Required PowerShell Host
PowerShellHostName = ''

# Minimum Host Version Required
PowerShellHostVersion = ''

# Minimum .NET Framework-Version
DotNetFrameworkVersion = ''

# Minimum CLR (Common Language Runtime) Version
CLRVersion = ''

# Processor Architecture Required (X86, Amd64, IA64)
ProcessorArchitecture = ''

# Required Modules (will load before this module loads)
RequiredModules = @()

# Required Assemblies
RequiredAssemblies = @()

# PowerShell Scripts (.ps1) that need to be executed before this module loads
ScriptsToProcess = @()

# Type files (.ps1xml) that need to be loaded when this module loads
TypesToProcess = @()

# Format files (.ps1xml) that need to be loaded when this module loads
FormatsToProcess = @()

# 
NestedModules = @()

# List of exportable functions
FunctionsToExport = '*'

# List of exportable cmdlets
CmdletsToExport = '*'

# List of exportable variables
VariablesToExport = '*'

# List of exportable aliases
AliasesToExport = '*'

# List of all modules contained in this module
ModuleList = @()

# List of all files contained in this module
FileList = @()

# Private data that needs to be passed to this module
PrivateData = ''

}