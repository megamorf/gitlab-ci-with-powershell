﻿function Invoke-GitlabCIJob
{
<#
.Synopsis
   Powershell wrapper for the gitlab runner exec command
.DESCRIPTION
   Invoke-GitlabCIJob needs to be called from the git repository 
   root directory that contains a valid .gitlab-ci.yml file
.EXAMPLE
   Invoke-GitlabCIJob -JobName 'my job name'
.EXAMPLE
   Invoke-GitlabCIJob -JobName 'my job name' -RunnerFilePath C:\gitlab\runners\gitlab-ci-multi-runner-windows-amd64.exe -Verbose
#>
    [CmdletBinding()]
    Param(
        
        [Parameter(Position=1)]
        $RunnerFilePath = "C:\Projects\runners\new\gitlab-ci-multi-runner-windows-amd64.exe"
    )
    DynamicParam 
    {
        Function Get-GitlabCIJob
        {
            [CmdletBinding()]
            Param($RepositoryRoot)

            try
            {
                $GitlabCiFile = Join-Path $RepositoryRoot '.gitlab-ci.yml' -Resolve -ErrorAction SilentlyContinue
                Write-Verbose "Scanning [$GitlabCiFile] for jobs..."

                # http://doc.gitlab.com/ce/ci/yaml/README.html
                $ReservedJobKeywords = @('before_script','image','services','stages','types','variables','cache')

                $Matches = Select-String -Path $GitlabCiFile -Pattern "(?<JobName>^[^(\s|#)+].*):"
                $Jobs = $Matches | % {$_.matches[0].groups['JobName'].Value} | Where-Object {$ReservedJobKeywords -notcontains $_} 

                Write-Verbose "$($Jobs.count) jobs found"
                $Jobs

            }
            catch
            {
                Write-Error "Gitlab CI file not found in $RepositoryRoot"
            }
        }
        if(Test-Path '.\.gitlab-ci.yml')
        {
            #create a new ParameterAttribute Object
            $JobNameAttribute = New-Object System.Management.Automation.ParameterAttribute
            $JobNameAttribute.Position = 0
            $JobNameAttribute.Mandatory = $true
            $JobNameAttribute.HelpMessage = "Gitlab CI job name"

            #create an attributecollection object for the attribute we just created.
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($JobNameAttribute)

            #create validateset from found jobs
            $Values = Get-GitlabCIJob -RepositoryRoot .
            $ValidateSet = New-object System.Management.Automation.ValidateSetAttribute($Values)
            $attributeCollection.Add($ValidateSet)

            #add our paramater specifying the attribute collection
            $JobNameParam = New-Object System.Management.Automation.RuntimeDefinedParameter('JobName', [string], $attributeCollection)
            
            #expose the name of our parameter
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
            $paramDictionary.Add('JobName', $JobNameParam)
            return $paramDictionary
        }
        
    }

    PROCESS
    {
        Write-Verbose "jobname [$($PSBoundParameters.JobName)]"
        
        if( $PSBoundParameters.JobName -and (-not [string]::IsNullOrEmpty($PSBoundParameters.JobName)) )
        {
            Write-Verbose "Parameters:"
            Write-Verbose "`tRunner:     $RunnerFilePath"
            Write-Verbose "`tJobName:    $JobName`n" 
            
            Write-Verbose "Starting runner job [$JobName]..."
            $cmd = "& '{0}' exec shell '{1}' --shell powershell" -f $RunnerFilePath,$JobName
            invoke-expression $cmd -Verbose

            #&$RunnerFilePath exec shell "$JobName" --shell powershell
        }
        else 
        {
            Write-Error "No gitlab ci configuration found"
        }
    }
}