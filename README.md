# Runner Configuration

## Runner Installation

Download the [latest runner](https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-windows-amd64.exe) on Windows:

```powershell
$url = 'https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-windows-amd64.exe'
$filepath = join-path $pwd 'gitlab-ci-runner.exe'
((new-object net.webclient).DownloadFile($url, $filepath))
```  

## Runner Registration

Go to the Gitlab CI website `<gitlab url>/ci` and Click the `Add project to CI` button. 
The project needs at least one build runner, click on the Runners section in the navigation pane to a runner to the project.

There are two types of runners - project specific and shared runners. 



### Prequisites

Create a new local user for the runner
```cmd
net user gitlab-ci-runner MySuperSecretPassword /ADD
```


Then register it unattended:

```


gitlab-ci-multi-runner register \
--non-interactive \
--url "https://ci.gitlab.com/" \
--registration-token "REGISTRATION_TOKEN" \
--description "gitlab-ci-ruby-2.1" \
--executor "shell" \

```



# Build Configuration

```
# Gitlab CI file
# http://doc.gitlab.com/ci/yaml/README.html
# syntax check (Lint) can be found under <gitlab ci url>/lint

# Predefined variables (Environment Variables)
# http://doc.gitlab.com/ci/variables/README.html
#
# Variable              Description
# CI                    Mark that build is executed in CI environment
# GITLAB_CI             Mark that build is executed in GitLab CI environment
# CI_SERVER             Mark that build is executed in CI environment
# CI_SERVER_NAME        CI server that is used to coordinate builds
# CI_SERVER_VERSION     Not yet defined
# CI_SERVER_REVISION    Not yet defined
# CI_BUILD_REF          The commit revision for which project is built
# CI_BUILD_BEFORE_SHA   The first commit that were included in push request
# CI_BUILD_REF_NAME     The branch or tag name for which project is built
# CI_BUILD_ID           The unique id of the current build that GitLab CI uses internally
# CI_BUILD_REPO         The URL to clone the Git repository
# CI_PROJECT_ID         The unique id of the current project that GitLab CI uses internally
# CI_PROJECT_DIR        The full path where the repository is cloned and where the build is ran

```